﻿using EveAI.Live;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eveHunter
{
    public partial class dlgSettings : Form
    {
        public dlgSettings()
        {
            InitializeComponent();
        }

        #region API
        private void lvAPI_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void bttnAPIAdd_Click(object sender, EventArgs e)
        {
            using (dlgAPI addAPI = new dlgAPI())
            {
                if (addAPI.ShowDialog(this) == DialogResult.OK) RefreshAPIList();
            }
        }

        private void bttnAPIDelete_Click(object sender, EventArgs e)
        {
            if (lvAPI.SelectedItems.Count > 0) foreach (ListViewItem aItem in lvAPI.SelectedItems) lvAPI.Items.Remove(aItem);
        }
        private bool RefreshAPIList()
        {
            lvAPI.Items.Clear();
            foreach (uAPI myAPI in Properties.Settings.Default.APIList) lvAPI.Items.Add(new ListViewItem(myAPI.Name));
            return true;
        }
        #endregion

    }
}
