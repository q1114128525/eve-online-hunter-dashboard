﻿using EveAI.Live;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace eveHunter
{
    public partial class frmDashboard : Form
    {
        public EveApi api;
        frmHUD HUD;
        public int hudcount = 1;
        Properties.Settings settings = Properties.Settings.Default;
        Dictionary<string, String> LayoutFiles = new Dictionary<string, string>();

        public frmDashboard()
        {
            InitializeComponent();
        }

        #region Methods/Functions
        private void GetFormDefaults()
        {
            if (settings.FormSize.Width == 0 || settings.FormSize.Height == 0)
            {
                // first start
                // optional: add default values
            }
            else
            {
                this.WindowState = settings.FormState;

                // we don't want a minimized window at startup
                if (this.WindowState == FormWindowState.Minimized) this.WindowState = FormWindowState.Normal;

                this.Location = settings.FormLocation;
                this.Size = settings.FormSize;
            }
            if (settings.Default_HUDs != "")
            {
                dockPanel1.LoadFromXml(settings.Default_HUDs.Split('|')[1], BuildDockContent);
            }
            else
            {
                if (System.IO.File.Exists(Application.StartupPath + @"\default.config"))
                {
                    dockPanel1.LoadFromXml(Application.StartupPath + @"\default.config", BuildDockContent);
                    settings.Default_HUDs = String.Format("default|{0}", Application.StartupPath + @"\default.config");
                    settings.Save();
                }
            }
        }

        private void SaveFormDefauts()
        {
            settings.FormState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
            {
                // save location and size if the state is normal
                settings.FormLocation = this.Location;
                settings.FormSize = this.Size;
            }
            else
            {
                // save the RestoreBounds if the form is minimized or maximized!
                settings.FormLocation = this.RestoreBounds.Location;
                settings.FormSize = this.RestoreBounds.Size;
            }
            if (dockPanel1.HasChildren)
            {
                try
                {
                    dockPanel1.SaveAsXml(Application.StartupPath + @"\default.config");
                    //settings.HUDs.Add("Default|" + Application.StartupPath + @"\default.config");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message.ToString());
                }
            }
            // don't forget to save the settings
            settings.Save();
        }

        private DockContent BuildDockContent(String persistString)
        {
            Console.WriteLine(persistString);

            foreach (frmHUD hud in dockPanel1.Contents)
            {
                if (hud.PersistString == persistString)
                {
                    return hud;
                }
            }
            return new frmHUD();
        }

        private void LoadDynamicMenuItems()
        {
            //layouts
            String[] configs = System.IO.Directory.GetFiles(Application.StartupPath, "*.config");
            if (configs.Length>0){
                foreach (String file in configs)
                {
                    LayoutFiles.Add(System.IO.Path.GetFileNameWithoutExtension(file), file);
                    layoutToolStripMenuItem.DropDownItems.Add(System.IO.Path.GetFileNameWithoutExtension(file));
                }
            }
            if (settings.Default_HUDs.Split('|')[0] != "") {
                foreach (ToolStripMenuItem layout in layoutToolStripMenuItem.DropDownItems)
                {
                    if (layout.Text == settings.Default_HUDs.Split('|')[0]) { layout.Checked = true; }
                    else { layout.Checked = false; }
                }
            }
        }
        #endregion

        #region Menu
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "exitToolStripMenuItem":
                    Application.Exit();
                    break;
                case "killmailToolStripMenuItem":
                    HUD = new frmHUD() { Name = "hud" + hudcount};
                    hudcount += 1;
                    HUD.Show(dockPanel1, DockState.Document);
                    break;
                case "bountyReportToolStripMenuItem":

                    break;
                case "settingsToolStripMenuItem":
                    using (dlgSettings Settings = new dlgSettings())
                    {
                        if (Settings.ShowDialog(this) == DialogResult.OK)
                        {

                        }
                    }
                    break;
                case "appInfoToolStripMenuItem":
                    using (AboutBox dlgAbout = new AboutBox()) dlgAbout.ShowDialog(this);
                    break;
                case "":

                    break;

            }
        }
        #endregion

        #region Form
        private void frmDashboard_Load(object sender, EventArgs e)
        {
            GetFormDefaults();
            LoadDynamicMenuItems();
            Text = String.Format("{0} v{1} by {2}", ProductName, ProductVersion, CompanyName);
            //HUD = new frmHUD() { Name = "hud" + hudcount, MdiParent = this }; HUD.Show(); hudcount += 1;

        }
        private void frmDashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveFormDefauts();
        }
        #endregion

    }
}
