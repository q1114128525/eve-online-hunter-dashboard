﻿namespace eveHunter
{
    partial class dlgSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpAPI = new System.Windows.Forms.GroupBox();
            this.lvAPI = new System.Windows.Forms.ListView();
            this.bttnAPIDelete = new System.Windows.Forms.Button();
            this.bttnAPIAdd = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.grpAPI.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 257);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // grpAPI
            // 
            this.grpAPI.Controls.Add(this.lvAPI);
            this.grpAPI.Controls.Add(this.bttnAPIDelete);
            this.grpAPI.Controls.Add(this.bttnAPIAdd);
            this.grpAPI.Location = new System.Drawing.Point(356, 12);
            this.grpAPI.Name = "grpAPI";
            this.grpAPI.Size = new System.Drawing.Size(200, 315);
            this.grpAPI.TabIndex = 0;
            this.grpAPI.TabStop = false;
            this.grpAPI.Text = "API";
            // 
            // lvAPI
            // 
            this.lvAPI.Location = new System.Drawing.Point(0, 19);
            this.lvAPI.Name = "lvAPI";
            this.lvAPI.Size = new System.Drawing.Size(200, 249);
            this.lvAPI.TabIndex = 3;
            this.lvAPI.UseCompatibleStateImageBehavior = false;
            this.lvAPI.View = System.Windows.Forms.View.List;
            this.lvAPI.DoubleClick += new System.EventHandler(this.lvAPI_DoubleClick);
            // 
            // bttnAPIDelete
            // 
            this.bttnAPIDelete.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnAPIDelete.Location = new System.Drawing.Point(109, 280);
            this.bttnAPIDelete.Name = "bttnAPIDelete";
            this.bttnAPIDelete.Size = new System.Drawing.Size(75, 23);
            this.bttnAPIDelete.TabIndex = 2;
            this.bttnAPIDelete.Text = "Delete";
            this.bttnAPIDelete.UseVisualStyleBackColor = true;
            this.bttnAPIDelete.Click += new System.EventHandler(this.bttnAPIDelete_Click);
            // 
            // bttnAPIAdd
            // 
            this.bttnAPIAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnAPIAdd.Location = new System.Drawing.Point(16, 280);
            this.bttnAPIAdd.Name = "bttnAPIAdd";
            this.bttnAPIAdd.Size = new System.Drawing.Size(75, 23);
            this.bttnAPIAdd.TabIndex = 1;
            this.bttnAPIAdd.Text = "Add";
            this.bttnAPIAdd.UseVisualStyleBackColor = true;
            this.bttnAPIAdd.Click += new System.EventHandler(this.bttnAPIAdd_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(12, 275);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(331, 128);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(372, 380);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(481, 380);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // dlgSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 415);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.grpAPI);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "dlgSettings";
            this.Text = "dlgSettings";
            this.grpAPI.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpAPI;
        private System.Windows.Forms.Button bttnAPIDelete;
        private System.Windows.Forms.Button bttnAPIAdd;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lvAPI;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}