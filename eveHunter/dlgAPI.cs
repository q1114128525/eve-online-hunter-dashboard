﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eveHunter
{
    public partial class dlgAPI : Form
    {
        Properties.Settings settings = Properties.Settings.Default;
        EveAI.Live.EveApi api;
        public uAPI myAPI;

        public dlgAPI()
        {
            InitializeComponent();
        }

        private void dlgAPI_Load(object sender, EventArgs e)
        {
            //load the chosen api info
            
            //check if its the default api info also

        }

        private void bttnCheckAPI_Click(object sender, EventArgs e)
        {
            
        }

        private void bttnClearAPI_Click(object sender, EventArgs e)
        {
            txtAPIName.Text = "";
            txtAPIKeyID.Text = "";
            txtAPIvCode.Text = "";
            cboCharaters.Items.Clear();
            lblExp.Text = "";
            lblAPIStatus.Text = "";
        }

        private void bttnSaveAPI_Click(object sender, EventArgs e)
        {
            //save to API list
            myAPI = new uAPI()
            {
                Name = txtAPIName.Text,
                userAPI = new EveAI.Live.EveApi(ProductName, long.Parse(txtAPIKeyID.Text), txtAPIvCode.Text)
            };
            
            //if (settings.APIList.Contains(txtAPIName.Text)) { }
            if (chkDefaultAPI.Checked)
            {
                //save as default API
            }
            settings.Save();
            Close();
        }

        private void bttnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
